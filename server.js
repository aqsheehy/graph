var open = require('open');
var express = require('express');
var routing = require('./routing.js');

var app = express();
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.static(__dirname + '/app'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
routing(app);

var server = app.listen(1337, function(){
	console.log('Server running at http://127.0.0.1:1337/');
	open('http://localhost:1337');
});