var posts = {};
var gets = {};

module.exports = {
	get: gets,
	post: posts,
	load: function(app){
		for (var i in gets)
			app.get(i, gets[i]);
		for (var j in posts)
			app.post(j, posts[j]);
	}
};