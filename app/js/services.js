'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('entityGraph.services', [])
	.value('version', '0.1')
	.factory('entityGraph', ['$http', function($http){
		var entityGraph;
		entityGraph = {
			Nodes: [
				{ key:0, "name":"Paris Parasadi","group":1},
				{ key:1, "name":"Tony Sheehy","group":1},
				{ key:2, "name":"GlobalX Legal Solutions", "group":2},
				{ key:3, "name":"ACN 004 085 616", "group":2},
				{ key:4, "name":"VIN 1HGCM82633A004352", "group":3},
				{ key:5, "name":"Registration 32432478324423", "group":4},
				{ key:6, "name":"Peter Maloney", "group": 1},
				{ key:7, "name":"AUSTRALIA AND. NEW ZEALAND BANKING GROUP LIMITED", "group": 5},
				{ key:8, "name":"ANZ", "group": 2}
			],
			Links: [
				{ source:0, target:1, type:"lives with", value: 1},
				{ source:1,target:2,type: "employed by", value: 1 },
				{ source:1,target:5,type: "grantor of", value: 1 },
				{ source:5,target:4,type: "registered security", value: 1 },
				{ source:5,target:7,type: "secured by", value: 1 },
				{ source:3,target:7,type: "member of", value: 1 },
				{ source:6,target:2,type: "director of", value: 1 },
				{ source:8,target:7,type: "member of", value: 1 }
			],
			Focus: null,
			SetFocus: function(node){
				entityGraph.Focus = node;
			},
			DropFocus: function(){
				entityGraph.Focus = null;
			},
			Load: function()
			{
				if (entityGraph.Focus)
				{
					var name = entityGraph.Focus.name;
					switch (entityGraph.Focus.group)
					{
						case 1:
							return { 
								$name: name,  
								$type: 'GlobalX.Common.DataModel.Individual',
								GivenNames: 'Anthony Quinn',
								Surname: 'Sheehy',
								DateOfBirth: new Date(1990, 4, 22)
							};
						case 2: 
							return {
								$name: name,  
								$type: 'GlobalX.Common.DataModel.Company',
								Name: 'GlobalX Legal Solutions',
								ACN: '004085616',
								ABN: '53004085616'
							};
						case 3:
							return {
								$name: name,
								$type: 'GlobalX.Common.DataModel.MotorVehicle',
								VIN: '1HGCM82633A004352',
								Numberplate: 'XYZ123'
							};
						case 4:
							return {
								$name: name,
								$type: 'GlobalX.Common.DataModel.CollateralRegistration',
								RegistrationNumber: '32432478324423',
								CollateralClass: 'MotorVehicle',
								CollateralType: 'Consumer',
								RegistrationEndDate: new Date(2021, 7, 23)
							};
						case 5:
							return {
								$name: name,
								$type: 'GlobalX.Common.DataModel.SecuredPartyGroup',
								Name: 'AUSTRALIA AND. NEW ZEALAND BANKING GROUP LIMITED',
								Token: '124352443223'
							};
					}
				}

				return null;
			},
			_cache: {},
			_dump: function(){
				delete entityGraph._cache;
				entityGraph._cache = {};
			}
		};
		return entityGraph;
	}]);
