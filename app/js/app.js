'use strict';

// Declare app level module which depends on filters, and services
angular.module('entityGraph', [
  'ngRoute',
  'ui.bootstrap',
  'entityGraph.filters',
  'entityGraph.services',
  'entityGraph.directives',
  'entityGraph.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/overview', { templateUrl: 'partials/graph.html', controller: 'GraphCtrl' });
  $routeProvider.otherwise({redirectTo: '/overview'});
}]);
