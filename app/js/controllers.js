'use strict';

/* Controllers */

angular.module('entityGraph.controllers', [])
	.controller('GraphCtrl', ['$scope', 'entityGraph', function($scope, entityGraph) {
		$scope.Graph = entityGraph;
		$scope.Focus = { }
		$scope.$watch('Graph.Focus', function(f, o)
		{
			$scope.Focus = entityGraph.Load(f);
		});
	}]);
