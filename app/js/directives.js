'use strict';

/* Directives */


angular.module('entityGraph.directives', [])
	.directive('appVersion', ['version', function(version) {
		return function(scope, elm, attrs) {
		  elm.text(version);
		};
	}])
	.directive('gxsCollateral', [function(){
		return {
			restrict: 'E',
			scope: {
				entity: '='
			},
			templateUrl: '/partials/nodes/collateral.html'
		};
	}])
	.directive('gxsCompany', [function(){
		return {
			restrict: 'E',
			scope: {
				entity: '='
			},
			templateUrl: '/partials/nodes/company.html'
		};
	}])
	.directive('gxsIndividual', [function(){
		return {
			restrict: 'E',
			scope: {
				entity: '='
			},
			templateUrl: '/partials/nodes/individual.html'
		};
	}])
	.directive('gxsMotorvehicle', [function(){
		return {
			restrict: 'E',
			scope: {
				entity: '='
			},
			templateUrl: '/partials/nodes/motorVehicle.html'
		};
	}])
	.directive('gxsProperty', [function(){
		return {
			restrict: 'E',
			scope: {
				entity: '='
			},
			templateUrl: '/partials/nodes/property.html'
		};
	}])
	.directive('gxsSecuredpartygroup', [function(){
		return {
			restrict: 'E',
			scope: {
				entity: '='
			},
			templateUrl: '/partials/nodes/securedPartyGroup.html'
		};
	}])
	.directive('gxsWatercraft', [function(){
		return {
			restrict: 'E',
			scope: {
				entity: '='
			},
			templateUrl: '/partials/nodes/watercraft.html'
		};
	}])
	.directive('egFocused', [function(){
		return {
			restrict: 'E',
			scope: {
				height: '=',
				width: '=',
				graph: '=',
				target: '='
			},
			templateUrl: '/partials/entityGraph/focused.html',
			link: function(s, e, a){

				var draw = function () {

					var entityGraph = s.graph;

					var sourceIndex = entityGraph.Nodes[s.target.key];

					var siblingLinks = [];
					for (var i = 0; i < entityGraph.Links.length; i++)
					{
						var candidate = entityGraph.Links[i];
						if (candidate.source.key == sourceIndex.key || candidate.target.key == sourceIndex.key)
							siblingLinks.push(candidate);
					}

					var uniqueNodes = [];
					for (var j = 0; j < siblingLinks.length; j++)
					{
						var candidateLink = siblingLinks[j];
						if (uniqueNodes.indexOf(candidateLink.source.key) == -1)
							uniqueNodes.push(candidateLink.source.key);
						if (uniqueNodes.indexOf(candidateLink.target.key) == -1)
							uniqueNodes.push(candidateLink.target.key);
					}

					var groupedNodes = [];
					for (var k = 0; k < uniqueNodes.length; k++)
					{
						var nodeKey = uniqueNodes[k];
						var node = entityGraph.Nodes[nodeKey];
						groupedNodes.push(node);
					}

					var width = s.width;
					var height = s.height;

					var force = d3.layout.force()
						.charge(-300)
					    .linkDistance(300)
					    .size([width, height]);

					var svgElem = angular.element('<svg width="' + width + '" height="' + height + '"></svg>');

					e.append(svgElem);

					var link;
					var node;
					var svg;

					var zoom = d3.behavior.zoom().scaleExtent([0, 8])
				        .on("zoom", function(){
							svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
						});

					svg =
						d3
						.select("svg")
						.call(zoom)
						.append("g");

				    force
				      .nodes(groupedNodes)
				      .links(siblingLinks)
				      .start();

			        var tick = function(){
			        	link.attr("x1", function(d) { return d.source.x; })
							.attr("y1", function(d) { return d.source.y; })
							.attr("x2", function(d) { return d.target.x; })
							.attr("y2", function(d) { return d.target.y; });

						node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
			        };

			        link = 
			        	svg
			        	.selectAll(".link")
				      	.data(siblingLinks)
				    		.enter().append("line")
				      		.attr("class", "link")
				      		.style("stroke-width", function(d) { return Math.sqrt(d.value); });

		      		node = svg
		      			.selectAll(".node")
		      			.data(groupedNodes)
		      				.enter().append("svg:g")
		      					.attr("class", "node")
		      					.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
		      					.call(force.drag);

					node.append("svg:circle")
						.attr("class", function(d) {  
							return "group-" + d.group;
						})
						.attr("r", function(d){
							if (s.target.key == d.key)
								return 20;
							return 10;
						})
						.on("mouseover", function(d) {
							if (s.target.key != d.key)
							{
								var circle = d3.select(this);
					            circle.transition().duration(100)
					                    .attr("r", 15);
							}
				        })
	                	.on("mouseout", function(d) {
	                		if (s.target.key != d.key)
							{
								var circle = d3.select(this);
					            circle.transition().duration(300)
					                    .attr("r", 10);
							}
				        })
						.on("click", function(d){
							entityGraph.SetFocus(d);
							s.target = d;
							svgElem.remove();
							draw();
							s.$apply();
						});

					node.append("svg:text")
						.attr("x", 20)
						.attr("y", 20)
						.attr("stroke", "black")
						.attr("font-size", "1.2em")
						.text(function(d) { 
							if (d.name.length > 20)
								return d.name.substring(0, 20) + '...'; 
							return d.name;
						});

			        force.on("tick", tick);
				};

				draw();
			}
		};
	}])
	.directive('egOverview', [function(){
		return {
			restrict: 'E',
			scope: {
				height: '=',
				width: '=',
				graph: '='
			},
			link: function(s, e, a){

				var entityGraph = s.graph;

				var width = s.width;
				var height = s.height;

				var force = d3.layout.force()
				    .charge(-120)
				    .linkDistance(20)
				    .size([width, height]);

				var svgElem = angular.element('<svg width="' + width + '" height="' + height + '"></svg>');

				e.append(svgElem);

				var link;
				var node;
				var svg;

				var tick = function(){
		        	link.attr("x1", function(d) { return d.source.x; })
						.attr("y1", function(d) { return d.source.y; })
						.attr("x2", function(d) { return d.target.x; })
						.attr("y2", function(d) { return d.target.y; });

					node.attr("cx", function(d) { return d.x; })
						.attr("cy", function(d) { return d.y; });
		        };

		        var zoom = d3.behavior.zoom().scaleExtent([0, 8])
			        .on("zoom", function(){
						svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
					});

				svg =
					d3
					.select("svg")
					.call(zoom)
					.append("g");

			    force
			      .nodes(entityGraph.Nodes)
			      .links(entityGraph.Links)
			      .start();

		        link = 
		        	svg
		        	.selectAll(".link")
			      	.data(entityGraph.Links)
			    		.enter().append("line")
			      		.attr("class", "link")
			      		.style("stroke-width", function(d) { return Math.sqrt(d.value); })
			      		.attr("transform", function(d) { return "translate(" + d + ")"; });

				node = svg.selectAll(".node")
					.data(entityGraph.Nodes)
					.enter().append("circle")
					.attr("class", function(d) {  
						return "node group-" + d.group;
					})
					.attr("r", 5)
					.call(force.drag)
					.on("mouseover", function() {
			            var circle = d3.select(this);
			            circle.transition().duration(100)
			                    .attr("r", 10);
			        })
                	.on("mouseout", function() {
			            var circle = d3.select(this);
			            circle.transition().duration(300)
			                    .attr("r", 5);
			        })
			        .on("click", function(d){
			        	entityGraph.SetFocus(d);
			        	s.$apply();
			        });

		        node
		         	.append("title")
      				.text(function(d) { return d.name; });

				force.on("tick", tick);
			}
		};
	}]);
